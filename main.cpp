/*
 * main.cpp
 *
 *  Created on: Oct 2, 2014
 *  Author: Luis A. Aguilera <laguiler@mail.usf.edu>
 *  Driver program of the class library for the class circularLinkedList
 *  that is used to generate a pointer-based linked list and run the
 *  Josephus algorithm on said list.
 *
 *  For more information on the Josephus Problem, please visit:
 *  http://en.wikipedia.org/wiki/Josephus_problem
 */

#include "circularLinkedList.h"
#include <string>
#define TEST_SIZE_ONE 10
#define TEST_POSITION_ONE 0
#define TEST_POSITION_TWO 3
#define TEST_POSITION_THREE 11

int main()
{
	//Create a list of integers.
	List<int> testListOne;
	int values = 0;

	//Populate the list.
	for(int i = 0; i < TEST_SIZE_ONE; ++i)
	{
		testListOne.insert(values, i);
		++values;
	}

	//Display the list
	cout << "Displaying the contents of the first list:" << endl;
	testListOne.display(cout);
	cout << endl;

	//Copy the list
	List<int> testListTwo = testListOne;
	cout << "\nDisplaying the contents of the copied list:" << endl;
	testListTwo.display(cout);
	cout << endl;

	//Testing the copy constructor
	List<int> testListThree(testListOne);
	cout << "\nDisplaying the contents of the list copied with the copy"
			"constructor: " << endl;
	testListThree.display(cout);


	//Remove specific elements from the list
	testListOne.remove(TEST_POSITION_ONE);
	testListOne.remove(TEST_POSITION_TWO);

	//Display the modified list using overloaded << operator
	cout << "\ntestListOne after removing two elements:" << endl;
	cout << testListOne;
	cout << endl;

	//Attempt to add an element at a position that is not allowed
	cout << "\nAttempting to add an element out of bounds." << endl;
	testListOne.insert(values, testListOne.getCapacity());

	//Attempt to remove an element at a position that is not allowed
	cout << "\nAttempting to remove an element out of bounds." << endl;
	testListOne.remove(TEST_POSITION_THREE);

	//Variable to hold the final value for the Josephus Algorithm.
	int finalValue = testListTwo.runJosephusAlgorithm();

	//Display the final value, our "lucky" rider
	cout << "\nThe final value after running the Josephus Algorithm on the first"
			"list is: " << finalValue << endl;

	//Creating another list list
	List<string> testListFour;

	//Populating the list manually
	testListFour.insert("John Doe",0);
	testListFour.insert("Jane Doe",1);
	testListFour.insert("Baby Doe", 2);
	testListFour.insert("Davy Crockett", 3);

	//Variable to hold the final value for the Josephus Algorithm
	string finalName = testListFour.runJosephusAlgorithm();

	//Display the final value of the second test
	cout << "\nThe rider's name after running the Josephus Algorithm on the second"
			"list is: " << finalName << endl;



}
