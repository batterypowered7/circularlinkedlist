/*
 * circularLinkedList.h
 *
 *  Created on: Oct 2, 2014
 *  Author: Luis A. Aguilera <laguiler@mail.usf.edu>
 *  Header file of the class library for the class circularLinkedList
 *  that is used to generate a pointer-based linked list and run the
 *  Josephus algorithm on said list.
 *
 *  For more information on the Josephus Problem, please visit:
 *  http://en.wikipedia.org/wiki/Josephus_problem
 */

#include <iostream>
#include <cstdlib>
#include <ctime>
using namespace std;

#ifndef CIRCULARLINKEDLIST_H_
#define CIRCULARLINKEDLIST_H_

template <typename T> class List;
template <typename T> std::ostream& operator<<(std::ostream&, const List<T>&);

template <class T>
class List
{
   private:

      class Node
	  {
         public:

            T data; //Data member of each element in the list
            Node * next; //Pointer to the next element of the list

      };

   public:

      List(); //Default constructor
      List(const List&); //Copy constructor
      ~List(); //Destructor

      bool isEmpty() const; //Function to check if the list is empty.
      bool insert(const T&, const int&); //Function to insert an element
      bool remove(const int&); //Function to remove an element

      const List& operator=(const List&);//necessary when using d.m.a.
      friend ostream& operator<< <>(ostream&, const List<T>&);

      int getSize() const; //Helper function to retrieve list size.
      int getCapacity() const;
      void display(ostream&) const; //Helper function to display list contents
      T runJosephusAlgorithm(); //Actual Josephus function

   private:

      Node * _first;//Pointer to the first element of the linked list
      int _size; //Variable to keep track of how many elements are in the list
      bool copyList(Node *, Node * &);
      bool deleteList(Node * firstPtr); //Helper function to delete the list
};

/**
 * Default Constructor: construct a list object
 *
 * Precondition: N/A
 * Postcondition: An empty list, with size 0, is created.
 */
template <class T>
List<T>::List()
{
   _first = NULL;
   _size = 0;
}

/**
 * Construct a copy of a List object.
 *
 * Precondition: A copy of originalList is needed. originalList is a const
 *    reference parameter
 * Postcondition: A copy of originalList has been constructed.
 */
template <class T>
List<T>::List(const List& originalList)
{
   // initialize an empty list
   _first = NULL;
   _size = originalList._size;

   // copy the original list
   copyList(originalList._first, _first);
}

/**
 * Destroy a list object
 *
 * Precondition: The life of the object is over.
 * Postcondition: The memory dynamically allocated by the linked list has been
 *    returned to the heap.
 */
template <class T>
List<T>::~List()
{
   _first->next = NULL;
   deleteList(_first);
}

/**
 * Check if a list is empty
 *
 * Precondition: N/A
 * Postcondition: Return true if list is empty and false otherwise
 */
template <class T>
bool List<T>::isEmpty() const
{
   return _first == NULL;
}

/**
 * Insert an item into the list at the specified position
 *
 * Preconditions: item is the value to be inserted; there is room in the array;
 *    0 <= position <= size
 * Postcondition: item is inserted into the list at the specified position.
 */
template <class T>
bool List<T>::insert(const T& value, const int& position)
{
   /* Input validation for negative numbers and positions that are too large
   if(position < 0 || position > getSize())
   {
      cout << "Error: Position out of bounds." << endl;
      cout << "Please select a position from 0 to " << getSize() << "\n? ";
      cin >> position;
      insert(value, position);
   }*/

   //Input validation for negative values for position.
   if(position < 0)
   {
      int newPosition;
      cout << "Error: Position out of bounds." << endl;
      cout << "Please select a position greater than or equal to 0\n?";
      cin >> newPosition;
      insert(value, newPosition);
   }

   // create a new node with the specified data
   Node * newNode = new Node;

   newNode->data = value;
   newNode->next = NULL;

   if (position == 0)
   {
      if (_first == NULL)
      {
         _first = newNode;
         newNode->next = newNode;
      }

      //Awful code to get insertion into the front of the list working.
      else
      {
    	 Node* finalElementPtr;
    	 finalElementPtr = _first;
    	 while(finalElementPtr->next != _first)
    	 {
    		 finalElementPtr = finalElementPtr->next;
    	 }

         newNode->next = _first;
         _first = newNode;
         finalElementPtr->next = _first;
      }
   }

   else
   {
      // go to the appropriate position in the linked list to insert the item
      Node * predPtr = _first;
      for (int i = 1; i < position; ++i)
      {
      	 if(predPtr->next == _first)
      	 {
            cout << "End of list reached. ";
            cout << "Position " << position << " invalid." << endl;
            return false;
      	 }

         predPtr = predPtr->next;
      }

      // insert the new element
      newNode->next = predPtr->next;
      predPtr->next = newNode;
   }

      // increment the size of the current list
      ++_size;

      return true;
}

/**
 * Remove the item at the specified position
 *
 * Preconditions: The list is not empty and 0 <= position < size
 * Postconditions: element at the specified position has been removed.
 */
template <class T>
bool List<T>::remove(const int& position)
{
   if(isEmpty())
   {
	   cout << "The list is empty!" << endl;
	   return false;
   }
   if(position < 0)
   {
      int newPosition;
      cout << "Error: Position out of bounds." << endl;
      cout << "Please select a position greater than or equal to 0\n?";
      cin >> newPosition;
      remove(newPosition);
   }

   // the first item is a special case
   //Awful, awful code to get it working properly. Oh God, why?
   if (position == 0)
   {
  	 Node* finalElementPtr;
  	 finalElementPtr = _first;
  	 while(finalElementPtr->next != _first)
  	 {
  		 finalElementPtr = finalElementPtr->next;
  	 }

      Node * ptr = _first;
      _first = ptr->next;
      finalElementPtr->next = _first;
      delete ptr;
   }

   else
   {
      // go to the appropriate position in the linked list to delete the item
      Node * ptr = _first;
      Node * predPtr;

      for (int i = 0; i < position; ++i)
      {
     	 if(ptr->next == _first)
     	 {
             cout << "End of list reached. ";
             cout << "Position " << position << " invalid." << endl;
     		 return false;
     	 }

         predPtr = ptr;
         ptr = ptr->next;

      }

      // bypass the item to be deleted
      predPtr->next = ptr->next;

      // free the memory for the item to be deleted
      delete ptr;
   }

   --_size;

   return true;
}

/**
 * Assign a copy of a list object to the current object
 *
 * Preconditions: N/A
 * Postconditions: A copy of rhs has been assigned to this object. A const
 *    reference to this list is returned.
 */
template <class T>
const List<T> & List<T>::operator =(const List & rhs)
{
   if(this == &rhs)
   {
      return *this;
   }

   _size = rhs._size;
   copyList(rhs._first, _first);
   return *this;
}

/**
 * Output operator for a list object
 *
 * Precondition: The ostream, out, is open
 * Postcondition: The list represented by the list object has been inserted into
 *    out
 */
template <typename T>
std::ostream& operator<<(std::ostream& out, const List<T>& list)
{
   list.display(out);

   return out;
}

/**
 * Get the size of a list object
 *
 * Precondition: N/A
 * Postcondition: The number of elements held in the list is returned.
 */
template <class T>
int List<T>::getSize() const
{
   return _size;
}

/**
 * Get the capacity of a list object.
 *
 * Note that this function is only provided for compatability with other list
 * implementations.
 *
 * Precondition: N/A
 * Postcondition: -1 is returned since a pointer based linked list has no
 *    predefined capacity
 */
template <class T>
int List<T>::getCapacity() const
{
	return -1;
}

/**
 * Output the list
 *
 * Precondition: The ostream, out, is open.
 * Postcondition: The list represented by this List object has been inserted
 *    into out.
 */
template <class T>
void List<T>::display(ostream & out) const
{
   Node * ptr = _first;

   while (ptr != NULL)
   {
      if(ptr->next == _first)
      {
    	  out << ptr->data << " ";
    	  ptr = ptr->next;
		  break;
      }

      out << ptr->data << " ";
      ptr = ptr->next;
   }
}

/**
 * Run the Josephus Algorithm
 *
 * Precondition: Linked list of n elements
 * Postcondition: A starting position between 0 and n - 1 is chosen randomly
 * A number of positions to shift is chosen between 0 and 2n is chosen randomly
 * Remove the node we end up at. Repeat the process until there is only one
 * node left, then return the value of that node.
 */
template <class T>
T List<T>::runJosephusAlgorithm()
{
   //Seed the random number generator
   srand (time(NULL));

   //Create a dummy list to copy the original list into
   List <T> dummyList;
   dummyList = *this;

   //Instantiate three pointers to help us traverse the list
   Node * previousPtr;
   Node * currentPtr;
   Node * tempPtr;

   //Because we are not allowed to use getSize(), we check to see if the first
   //node points to itself.
   while(dummyList._first != dummyList._first->next)
   {

	  previousPtr = NULL;
	  currentPtr = dummyList._first;

	  //Randomly generate the values for position and count
      int position = rand() % dummyList.getSize();
      int count = rand() % (dummyList.getSize() * 2) +1;

      //First walk to our starting position
      for(int i = 0; i < position; ++i)
      {
         previousPtr = currentPtr;
         currentPtr = currentPtr->next;
      }

      //Then shift positions until we have met our count
      for(int j = 0; j < count; ++j)
      {
         previousPtr = currentPtr;
         currentPtr = currentPtr->next;
      }

      //Special, awful code if we are deleting the first element
      //It was late and I couldn't get it to work in a more elegant manner
      if(currentPtr == dummyList._first)
      {
         Node* finalElementPtr;
         finalElementPtr = dummyList._first;

         while(finalElementPtr->next != dummyList._first)
         {
            finalElementPtr = finalElementPtr->next;
         }

    	 Node * ptr = dummyList._first;
    	 dummyList._first = ptr->next;
    	 finalElementPtr->next = dummyList._first;
    	 delete ptr;

    	 --dummyList._size;
      }

      //Regular, nice code for deleting an element that is not the first node
      else
      {
         tempPtr = currentPtr;
         previousPtr->next = currentPtr->next;

         delete tempPtr;

         --dummyList._size;
      }
   }

   /*Testing it with the "easy" implementation. If for some God forsaken
    * reason the above method stops working, please test this one for
    * partial credit.

   while(dummyList._first != dummyList._first->next)
   {
	   int position = rand() % dummyList.getSize();
	   int count = rand() % (dummyList.getSize() * 2);
	   int safe = (position + count) % dummyList.getSize();
	   dummyList.remove(safe);
   }*/

   return dummyList._first->data;
}

/**
 * Copy a linked list
 *
 * Precondition: originalList is the list to be copied
 * Postcondition: copiedList points to a copy of originalList
 */
template <class T>
bool List<T>::copyList(Node * originalListFirst, Node * &copiedListFirst)
{
   // if the original list is empty, then simply delete the anything held in
   // the copied list
   if (originalListFirst == NULL)
   {
      deleteList(copiedListFirst);
      return true;
   }

   // if there is already a list held where the copied list should go, then
   // delete that list
   deleteList(copiedListFirst);

   // copy the data from the first node
   copiedListFirst = new Node;
   copiedListFirst->data = originalListFirst->data;
   copiedListFirst->next = copiedListFirst;

   // create a pointer to the current location in each list
   Node * originalPtr = originalListFirst;
   Node * copiedPtr = copiedListFirst;

   // copy the data in the remaining nodes
   while (originalPtr->next != originalListFirst)
   {
      copiedPtr->next = new Node;
      originalPtr = originalPtr->next;
      copiedPtr = copiedPtr->next;

      copiedPtr->data = originalPtr->data;
      copiedPtr->next = copiedListFirst;
   }

   return true;
}

/**
 * Delete a linked list
 *
 * Precondition: The life of the linked list is over
 * Postcondition: The memory dynamically allocated by each node of the linked
 *    list is returned to the heap.
 */
template <class T>
bool List<T>::deleteList(Node * firstPtr)
{
   if(firstPtr == NULL)
   {
      return true;
   }

   Node * ptr = firstPtr;
   Node * tempPtr;

   while(ptr != NULL)
   {
      tempPtr = ptr;
      ptr = ptr->next;
      delete tempPtr;
   }

   firstPtr = NULL;
   return true;
}

#endif /* CIRCULARLINKEDLIST_H_ */
